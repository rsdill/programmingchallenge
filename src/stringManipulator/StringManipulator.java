package stringManipulator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import stringManipulator.SortByWeight;

public class StringManipulator {

	public static String cleanString(String str){
		//use regex to remove all non-alphabetic chars				
		str = str.replaceAll("[^a-zA-Z]", "").toUpperCase();
		
		return str;
	}
	
	public static boolean hasUniqueChars(String word){
		boolean bool = true;
		ArrayList<Character> charList = new ArrayList<Character>();
		
		//put string into char array
		char[] stringArray = word.toCharArray();
		
		//loop through char array
		for(char c : stringArray){
			//if list contains char then it is duplicate. if not add
			if(charList.contains(c)){
				bool = false;
				return bool;				
			}else{
				charList.add(c);
			}
		}		
					
		return bool;
	}
	
	public static float getWeight(String word){
		float weight;
		float sum = 0;		
		
		//put string into char array
		char[] stringArray = word.toCharArray();		
		
		//loop through array to get ascii value of chars
		for(char c:stringArray){			
			float ascii = (float) c;			
			//add value to sum
			sum = sum + ascii;			
		}
		
		//get average
		weight = sum/stringArray.length;
		
		return weight;
	}
	
	public static ArrayList<StringResult> sortStrings(ArrayList<String> words){
		//create new array of StringResult
		ArrayList<StringResult> resultArray = new ArrayList<StringResult>();
		
		//loop through word array
		for(String word:words){
			//clean string
			word = cleanString(word);
			//create String result and set values
			StringResult sr = new StringResult();
			sr.setWord(word);
			sr.setUnique(hasUniqueChars(word));
			sr.setWeight(getWeight(word));
			
			//add to list
			resultArray.add(sr);			
		}
		
		//sort list by weight
		Collections.sort(resultArray, new SortByWeight());
		
		return resultArray;		 
	}
	
	public static void main(String[] args) throws IOException {

		ArrayList<StringResult> result = new ArrayList<StringResult>();
		ArrayList<String> words = new ArrayList<String>();
		String csv = "src/atdd/challenge_tests.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplit = ",";
		String fileHeader = "Words,Unique,Weight";
		String newLine = "\n";
				
		FileWriter fileWriter = new FileWriter(new File("src/atdd/","challenge_sorted.csv"));
		
		try{
			
			//append head with new line
			fileWriter.append(fileHeader);
			fileWriter.append(newLine);
			
			//read from csv file
			br = new BufferedReader(new FileReader(csv));
			//skip header
			br.readLine();
			
			while((line = br.readLine()) != null){
				words.add(line);					
							
			}
			
			//send to sort and get results			
			result = sortStrings(words);
			
			//loop through and add results to file
			for(StringResult sr: result){
				fileWriter.append(sr.getWord());
				fileWriter.append(csvSplit);
				fileWriter.append(String.valueOf(sr.getUnique()));
				fileWriter.append(csvSplit);
				fileWriter.append(String.valueOf(sr.getWeight()));
				fileWriter.append(newLine);				
			}
			
			
		}catch(FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			//close resources
			try{
				if(br !=null){
					br.close();
				}
				fileWriter.flush();
				fileWriter.close();
			}catch(IOException e){
					e.printStackTrace();
			}			
		}		
	}
}
