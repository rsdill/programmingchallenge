package stringManipulator;

public class StringResult {
	private String word;
	private boolean unique;
	private float weight;
	
	public String getWord(){
		return word;
	}
	public void setWord(String word){
		this.word=word;
	}
	
	public boolean getUnique(){
		return unique;
	}
	public void setUnique(boolean unique){
		this.unique=unique;
	}
	
	public float getWeight(){
		return weight;
	}
	public void setWeight(float weight){
		this.weight=weight;
	}
	
	public String toString(){
		return this.word+" "+this.unique+" "+this.weight;
	}
		
}


