package stringManipulator;

import java.util.Comparator;

public class SortByWeight implements Comparator<StringResult>{
	
	public int compare(StringResult a,StringResult b){
		
		return (int) (a.getWeight() - b.getWeight());
	}
}
